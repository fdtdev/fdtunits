﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.Units
{
    [DisallowMultipleComponent]
    public class SkinnedRendererPlugin : UnitPlugin
    {
        [SerializeField, RedNull] protected SkinnedMeshRenderer _skinnedMeshRenderer;
        [SerializeField, RedNull] protected Animator _anim;
        public Animator Anim
        {
            get { return _anim; }
        }
        [SerializeField, Layer] protected int _visibleLayer;
        [SerializeField, Layer] protected int _invisibleLayer;
        [SerializeField] protected bool _visible = true;
        protected int _currentLayer;
        public int VisibleLayer
        {
            get { return _visibleLayer; }
        }
        public int InvisibleLayer
        {
            get { return _invisibleLayer; }
        }
        public SkinnedMeshRenderer SkinnedMeshRenderer
        {
            get { return _skinnedMeshRenderer; }
        }

        protected override void OnSetData()
        {

        }

        public void SetVisible(bool v)
        {
            _visible = v;
            SetVisibleLayers();

        }

        protected void SetVisibleLayers()
        {
            if (_visible)
            {
                _currentLayer = _visibleLayer;
            }
            else
            {
                _currentLayer = _invisibleLayer;
            }

            MoveToLayer(_skinnedMeshRenderer.transform, _currentLayer);
        }

        void MoveToLayer(Transform root, int layer)
        {
            root.gameObject.layer = layer;
            foreach (Transform child in root)
                MoveToLayer(child, layer);
        }
        
        public void SetMorphValue(string morph, float value)
        {
            var blendShapeIndex = _skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex(morph);
            _skinnedMeshRenderer.SetBlendShapeWeight(blendShapeIndex, value);
        }

        public override void Init()
        {
            base.Init();
            SetVisibleLayers();
        }
    }
}