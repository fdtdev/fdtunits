﻿using System;
using System.Collections.Generic;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.Units
{
    public class Unit : MonoBehaviour
    {
        public Action<Unit, UnitPlugin> OnInitializedPlugin;
        public Action<Unit> OnInitializedPlugins;
        
        [System.Serializable]
        public class GroupData
        {
            public IDAsset _groupID;
            public List<UnitPlugin> _plugins = new List<UnitPlugin>();
            
            public void AddPlugin(UnitPlugin pluginObj)
            {
                if (HasPlugin(pluginObj))
                {
                    _plugins.Add(pluginObj);
                }
            }

            public void RemovePlugin(UnitPlugin pluginObj)
            {
                if (HasPlugin(pluginObj))
                {
                    _plugins.Remove(pluginObj);
                }
            }

            public bool HasPlugin(UnitPlugin pluginObj)
            {
                return _plugins.Contains(pluginObj);
            }
            public void Execute<T>(Action<T> callback) where T:UnitPlugin
            {
                for (int i = 0; i < _plugins.Count; i++)
                {
                    callback(_plugins[i] as T);
                }
            }

            public void Execute(Action<UnitPlugin> callback)
            {
                for (int i = 0; i < _plugins.Count; i++)
                {
                    callback(_plugins[i]);
                }
            }
        }
        [SerializeField] protected List<UnitPlugin> _unitPlugins;
        #if UNITY_EDITOR
        public List<UnitPlugin> UnitPlugins
        {
            get { return _unitPlugins; }
        }
        #endif
        [SerializeField] protected List<GroupData> _groups;
        protected virtual void OnEnable()
        {
            FeedPlugins();
        }

        
        public void FeedPlugins()
        {
            foreach (var plugin in _unitPlugins)
            {
                FeedPlugin(plugin);
            }
        }

        public T AddPlugin<T>() where T : UnitPlugin
        {
            T c = gameObject.AddComponent<T>();
            RegisterPlugin(c);
            FeedPlugin(c);
            InitPlugin(c);
            return c;
        }

        protected void InitPlugin<T>(T unitPlugin) where T : UnitPlugin
        {
            unitPlugin.Init();
            OnInitializedPlugin?.Invoke(this, unitPlugin);
        }

        public T AddPlugin<T>(T prefab) where T : UnitPlugin
        {
            T c = Instantiate(prefab);
            RegisterPlugin(c);
            FeedPlugin(c);
            InitPlugin(c);
            return c;
        }
        public bool RemovePlugin<T>() where T : UnitPlugin
        {
            T p = GetPlugin<T>();
            if (p != null)
            {
                UnregisterPlugin(p);
                if (p.gameObject != gameObject)
                    Destroy(p.gameObject);
                else
                    Destroy(p);

                return true;
            }

            return false;
        }

        public T GetPlugin<T>() where T : UnitPlugin
        {
            foreach (var plugin in _unitPlugins)
            {
                if (plugin is T)
                    return plugin as T;
            
            }
            return null;
        }

        public void FeedPlugin(UnitPlugin plugin)
        {
            plugin.SetData(this);
            
        }

        public void Init()
        {
            OnInit();
            foreach (var plugin in _unitPlugins)
            {
                InitPlugin(plugin);
            }
            OnInitializedPlugins?.Invoke(this);
        }

        protected virtual void OnInit()
        {
       
        }

        public bool HasPlugin(UnitPlugin pluginObj)
        {
            
            if (pluginObj.IsPrefab())
            {
                string n = pluginObj.name;
                for (int i = 0; i < _unitPlugins.Count; i++)
                {
                    if (_unitPlugins[i].name == pluginObj.name)
                    {
                        return true;
                    }
                }
            }
            return _unitPlugins.Contains(pluginObj);
        }

        public bool HasPlugin<T>() where T : UnitPlugin
        {
            foreach (var plugin in _unitPlugins)
            {
                if (plugin is T)
                    return true;

            }
            return false;
        }
        public void RegisterPlugin(UnitPlugin pluginObj)
        {
            _unitPlugins.Add(pluginObj);
        }

        public void UnregisterPlugin(UnitPlugin pluginObj)
        {
            _unitPlugins.Remove(pluginObj);
        }

        public void AddToGroup(UnitPlugin pluginObj, IDAsset group)
        {
            if (HasPlugin(pluginObj))
            {
                GroupData data = GetGroupData(group);
                if (data != null)
                {
                    data.AddPlugin(pluginObj);
                }
            }
        }

        public GroupData GetGroupData(IDAsset @group)
        {
            for (int i = 0; i < _groups.Count; i++)
            {
                if (_groups[i]._groupID == @group)
                {
                    return _groups[i];
                }
            }
            return null;
        }

        private void Update()
        {
            for (int i = 0; i < _unitPlugins.Count; i++)
            {
                _unitPlugins[i].PluginUpdate();
            }
            OnUpdate();
        }

        protected virtual void OnUpdate()
        {
            
        }
        private void FixedUpdate()
        {
            for (int i = 0; i < _unitPlugins.Count; i++)
            {
                _unitPlugins[i].PluginFixedUpdate();
            }
            OnFixedUpdate();
        }

        protected virtual void OnFixedUpdate()
        {
            
        }
    }
}
