﻿using UnityEngine;

namespace com.FDT.Units
{
    public abstract class UnitPlugin : MonoBehaviour
    {
        [SerializeField] protected Unit _unit;
        public Unit Unit
        {
            get { return _unit; }
        }

        public void SetData(Unit unit)
        {
            _unit = unit;
            OnSetData();
        }

        protected abstract void OnSetData();

        public virtual void Init()
        {
            
        }
        protected virtual void OnEnable()
        {
        }
        protected virtual void OnDisable()
        {
        }

        public virtual void PluginUpdate()
        {
        }
        public virtual void PluginFixedUpdate()
        {
        }
    }
}