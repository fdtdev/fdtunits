﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.Units
{
    public abstract class UnitPluginRegistered<TRegisterer, TRegistrable, TRegistrableId> : UnitPlugin,
        IRegistrable<TRegistrableId>
        where TRegisterer : IRegisterer<TRegistrable, TRegistrableId>
        where TRegistrable : class, IRegistrable<TRegistrableId>
        where TRegistrableId : IRegistrableID
    {
        [SerializeField] protected TRegisterer _actionReferenceList;
        [SerializeField] protected TRegistrableId _referenceID;

        protected override void OnEnable()
        {
            base.OnEnable();
            _actionReferenceList.Register(((Object) this) as TRegistrable);
        }

        protected override void OnDisable()
        {
            _actionReferenceList.Unregister(((Object) this) as TRegistrable);
            base.OnDisable();
        }

        public TRegistrableId referenceID
        {
            get { return _referenceID; }
        }
    }
}