﻿using com.FDT.Common.Editor;
using com.FDT.Common.RotorzReorderableList;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Units.Editor
{
    [CustomPropertyDrawer(typeof(Unit.GroupData))]
    public class GroupDataDrawer : UnityEditor.PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, 16), property.FindPropertyRelative("_groupID"), true);
            SerializedProperty listProp = property.FindPropertyRelative("_plugins");
            float h = listProp.GetHeight();
            EditorGUI.PropertyField(new Rect(position.x, position.y + 18, position.width, h), listProp, true);
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty listProp = property.FindPropertyRelative("_plugins");
            float h = listProp.GetHeight();
            return h + 22;
        }
    }
    /*[CustomPropertyDrawer(typeof(UnitPlugin))]
    public class UnitPluginDrawer : UnityEditor.PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            Color oldColor = GUI.backgroundColor;
            if (!property.FindPropertyRelative("m_Enabled").boolValue)
            //if (!((UnitPlugin) property.serializedObject.targetObject).enabled)
            {
                GUI.backgroundColor = Color.grey;
            }
            EditorGUI.PropertyField(position, property, true);
            GUI.backgroundColor = oldColor;
            EditorGUI.EndProperty();
        }
    }*/
    [CustomEditor(typeof(Unit), true)]
    public class UnitEditor : UnityEditor.Editor
    {
        private SerializedProperty _pluginsProperty;
        private CallbackSerializedPropertyAdaptor _pluginsListAdaptor;
        private ReorderableListControl _pluginsList;
        
        private SerializedProperty _groupsProperty;
        private CallbackSerializedPropertyAdaptor _groupsListAdaptor;
        private ReorderableListControl _groupsList;

        protected Color oldBackColor;
        private void OnEnable()
        {
            oldBackColor = GUI.backgroundColor;
            _pluginsProperty = serializedObject.FindProperty("_unitPlugins");
            _pluginsListAdaptor = new CallbackSerializedPropertyAdaptor(_pluginsProperty, 0);
            _pluginsListAdaptor.OnBeforeDrawItem += (rect, i) =>
            {
                if ((target as Unit).UnitPlugins[i] == null || !(target as Unit).UnitPlugins[i].enabled)
                {
                    GUI.backgroundColor = Color.gray;
                }
            };
            _pluginsListAdaptor.OnAfterDrawItem += (rect, i) => { GUI.backgroundColor = oldBackColor; };
            _pluginsList = new ReorderableListControl();
            
            _groupsProperty = serializedObject.FindProperty("_groups");
            _groupsListAdaptor = new CallbackSerializedPropertyAdaptor(_groupsProperty, 0);
            _groupsList = new ReorderableListControl();
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.Space();
            GUILayout.Box("Plugins should go into the same gameObject as their Unit, or in a separate gameObject alone. In the second case, the gameObject would be destroyed on removal.", EditorStyles.helpBox);
            EditorGUILayout.Space();
            ReorderableListGUI.Title("Plugins");
            _pluginsList.Draw(_pluginsListAdaptor);
            EditorGUILayout.Space();
            ReorderableListGUI.Title("Groups");
            _pluginsList.Draw(_groupsListAdaptor);
            EditorGUILayout.Space();
            if (GUILayout.Button("Get Plugins"))
            {
                Unit u = target as Unit;
                var a = u.GetComponentsInChildren<UnitPlugin>();
                var p = serializedObject.FindProperty("_unitPlugins");
                p.arraySize = a.Length;
                for (int i = 0; i < a.Length; i++)
                {
                    p.GetArrayElementAtIndex(i).objectReferenceValue = a[i];
                    SerializedObject o = new SerializedObject(a[i]);
                    o.FindProperty("_unit").objectReferenceValue = target;
                    o.ApplyModifiedProperties();
                }
                serializedObject.ApplyModifiedProperties();
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}