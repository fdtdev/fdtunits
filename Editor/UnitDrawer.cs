﻿using System.Reflection;
using UnityEditor;
using UnityEngine;
using com.FDT.Common;

namespace com.FDT.Units.Editor
{
    [CustomPropertyDrawer(typeof(Unit), true)]
    public class UnitDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            if ((property.serializedObject.targetObject is Component) &&
                !(property.serializedObject.targetObject as Component).IsPrefab())
            {
                UnitPlugin pluginObj = property.serializedObject.targetObject as UnitPlugin;
                
                float getWidth = 50;
                Rect r1 = new Rect(position.x, position.y, position.width - getWidth, position.height);
                Rect r2 = new Rect(position.x + r1.width, position.y, getWidth, position.height);
                EditorGUI.PropertyField(r1, property, true);
                if (GUI.Button(r2, "GET"))
                {
                    Unit u = (property.serializedObject.targetObject as Component).gameObject.GetComponent<Unit>();
                    if (u == null)
                    {
                        u = (property.serializedObject.targetObject as Component).gameObject.GetComponentInParent<Unit>();
                    }
                    if (u != null)
                    {
                        if (pluginObj != null && !u.HasPlugin(pluginObj))
                        {
                            u.RegisterPlugin(pluginObj);
                            EditorUtility.SetDirty(u);
                        }
                        property.objectReferenceValue = u;
                        property.serializedObject.ApplyModifiedProperties();
                        property.serializedObject.Update();
                    }
                    
                }
            }
            else
            {
                EditorGUI.PropertyField(position, property, true);
            }

            EditorGUI.EndProperty();
        }
    }
}