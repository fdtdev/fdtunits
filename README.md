# FDT Units

Unit system with plugins


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.units": "https://bitbucket.org/fdtdev/fdtunits.git#2.1.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtunits/src/2.1.0/LICENSE.md)